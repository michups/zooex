package pl.codementors.zoo;

import pl.codementors.zoo.animals.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Application main class.
 *
 * @author psysiu
 */
public class ZooMain {

    /**
     * Application starting method.
     *
     * @param args Application starting params.
     */
    public static void main(String[] args) {

        Map<Owner, List<Animal>>  ownersAnimals = new TreeMap<>(new ReversStringComparator());
        List<Animal> animals = new ArrayList<>();

        try (InputStream is = ZooMain.class.getResourceAsStream("/pl/codementors/zoo/input/animals.txt");
             Scanner scanner = new Scanner(is);) {
            while (scanner.hasNext()) {
                String ownerName = scanner.next();
                String ownerType = scanner.next();
                Owner owner = new Owner(ownerName, Owner.Type.valueOf(ownerType));
                String type = scanner.next();
                String name = scanner.next();
                String color = scanner.next();

                if(!(ownersAnimals.containsKey(owner))){
                    ownersAnimals.put(owner, new ArrayList<Animal>()) ;
                }

                switch (type) {
                    case "Bird": {
                        animals.add(new Bird(name, color));
                            ownersAnimals.get(owner).add(new Bird(name, color));
                        break;
                    }
                    case "Lizard": {
                        animals.add(new Lizard(name, color));
                            ownersAnimals.get(owner).add(new Lizard(name, color));
                        break;
                    }
                    case "Mammal": {
                        animals.add(new Mammal(name, color));
                            ownersAnimals.get(owner).add(new Mammal(name, color));
                        break;
                    }
                    case "Dolphin": {
                        animals.add(new Dolphin(name, color));
                            ownersAnimals.get(owner).add(new Dolphin(name, color));
                        break;
                    }
                    case "Monkey": {
                        animals.add(new Monkey(name, color));
                            ownersAnimals.get(owner).add(new Monkey(name, color));
                        break;
                    }
                }
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
        Set<Mammal>   mammalSet = new HashSet<>();
        List<Bird>   birdList = new ArrayList<>();
        Collection<Lizard> lizardCollection =  new TreeSet<>();
        for (Animal a: animals ) {
            if(a instanceof Mammal){
                mammalSet.add((Mammal)a);
            }
            if(a instanceof Bird){
                birdList.add((Bird)a);
            }
            if(a instanceof Lizard){
                lizardCollection.add((Lizard)a);
            }
        }



        System.out.println();
        System.out.println("---------HASH SET-----------");
        Set<Animal> animalsSet = new HashSet<Animal>();//uses equals method
        animalsSet.addAll(animals);
        printAllAnimals(animalsSet);

        System.out.println();
        System.out.println("---------TREE SET-----------");
        Set<Animal> animalsTreeSet = new TreeSet<Animal>(new MyComparator());//uses
        animalsTreeSet.addAll(animals);
        printAllAnimals(animalsTreeSet);

        System.out.println();
        System.out.println("----------SORT AND PRINT----------");
        Collections.sort(animals);
        printAllAnimals(animals);

        System.out.println();
        System.out.println("---------COMPER BY MY COMPARATOR-----------");
        Collections.sort(animals,  new MyComparator());
        printAllAnimals(animals);

        System.out.println();
        System.out.println("---------ALL COLECTIONS-----------");
        printAllAnimals(mammalSet, birdList , lizardCollection );

        System.out.println();
        System.out.println("---------KEY VALUE-----------");
        printAllAnimalsAndOwnders(ownersAnimals);


    }
    public static void printAllAnimals(Collection<? extends Animal>...  animals){

        for (Collection<? extends Animal> animalCollection :animals ) {

            System.out.println("----------------------------");
            for (Animal animal : animalCollection) {
                System.out.println(animal);
            }
        }

    }
    public static void printAllAnimalsAndOwnders(Map<Owner, List<Animal>>  ownersAnimals  ){

            for (Map.Entry<Owner, List<Animal>> entry : ownersAnimals.entrySet()) {
                System.out.print(entry.getKey() +" -> [");
                boolean firsAnimal = true;
                for (Animal animal : entry.getValue()) {
                    if(firsAnimal != true){
                        System.out.print(", ");
                    }else {
                        firsAnimal = false;
                    }

                    System.out.print( animal.toString());

                }
                System.out.println("]");
            }

    }


}
