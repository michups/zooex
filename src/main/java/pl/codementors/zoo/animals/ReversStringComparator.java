package pl.codementors.zoo.animals;

import java.util.Comparator;

/**
 * Created by michups on 14.06.17.
 */
public class ReversStringComparator implements Comparator<Owner>{

    @Override
    public int compare(Owner o1, Owner o2) {

         return o1.compareTo(o2)*(-1);

    }


}
