package pl.codementors.zoo.animals;

/**
 * Created by michups on 13.06.17.
 */
public class Monkey extends  Mammal {

    public Monkey(String name, String furColor) {
        super(name,furColor);
    }

    @Override
    public String toString(){
        return "Monkey [Name: "+ getName()+", Color: "+ getFurColor()+" ]";
    }


    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Monkey) {
            return super.equals(obj) ;
        }
        else
            return false;
    }
}
