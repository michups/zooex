package pl.codementors.zoo.animals;

/**
 * Created by michups on 13.06.17.
 */
public class Dolphin extends Mammal {

    public Dolphin(String name, String furColor) {
        super(name,furColor);
    }

    @Override
    public String toString(){
        return "Dolphin [Name: "+ getName()+", Color: "+ getFurColor()+" ]";
    }




    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Dolphin) {
            return super.equals(obj);
        } else {
            return false;
        }
    }
}
