package pl.codementors.zoo.animals;

import java.util.Comparator;

/**
 * Created by michups on 13.06.17.
 */
public class MyComparator implements Comparator<Animal> {

    @Override
    public int compare(Animal o1, Animal o2) {

        if ( o1.getClass().getSimpleName().equals(o2.getClass().getSimpleName())){

            return o1.getName().compareTo(o2.getName());
        }
        else
        return o1.getClass().getSimpleName().compareTo(o2.getClass().getSimpleName());

    }


}
