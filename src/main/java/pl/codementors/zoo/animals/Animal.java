package pl.codementors.zoo.animals;

/**
 * Just an animal.
 *
 * @author psysiu
 */
public class Animal implements Comparable<Animal>{

    /**
     * Animal name.
     */
    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return "Animal [Name: "+ name+" ]";
    }


    public int compareTo(Animal animal){

        if ( this.getClass().getCanonicalName().equals(animal.getClass().getCanonicalName())){

            return this.getName().compareTo(animal.getName());
        } else{
            return this.getClass().getCanonicalName().compareTo(animal.getClass().getCanonicalName());
        }
    }

    @Override
    public int hashCode() {
        return name.hashCode();

    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj instanceof Animal) {
            return this.name.equals(((Animal) obj).getName());
        } else {
            return false;
        }
    }
}
