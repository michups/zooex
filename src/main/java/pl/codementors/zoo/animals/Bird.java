package pl.codementors.zoo.animals;

/**
 * Just a bird with feathers.
 *
 * @author psysiu
 */
public class Bird extends Animal {

    /**
     * Color of the bird feathers.
     */
    private String featherColor;

    public Bird(String name, String featherColor) {
        super(name);
        this.featherColor = featherColor;
    }

    public String getFeatherColor() {
        return featherColor;
    }

    public void setFeatherColor(String featherColor) {
        this.featherColor = featherColor;
    }


    @Override
    public String toString(){
        return "Bird [Name: "+ getName()+", Color: "+ featherColor+" ]";
    }

    @Override
    public int hashCode() {
        return super.hashCode() + 56 * featherColor.hashCode();

    }

    @Override
    public int compareTo(Animal animal){

        int defCompare = super.compareTo(animal);
        if(defCompare!=0){
            return defCompare;
        }else if(animal instanceof Bird) {
            return featherColor.compareTo(((Bird) animal).getFeatherColor());
        }else{
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Bird) {
            return super.equals(obj) && featherColor.equals(((Bird)obj).featherColor);
        }
        else
            return false;
    }
}
