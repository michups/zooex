package pl.codementors.zoo.animals;

/**
 * Just a mammal with some fur.
 *
 * @author psysiu
 */
public class Mammal extends Animal {

    /**
     * Color of the animal fur.
     */
    private String furColor;

    public Mammal(String name, String furColor) {
        super(name);
        this.furColor = furColor;
    }

    public String getFurColor() {
        return furColor;
    }

    public void setFurColor(String furColor) {
        this.furColor = furColor;
    }


    @Override
    public String toString(){
        return "Mammal [Name: "+ getName()+", Color: "+ furColor+" ]";
    }

    @Override
    public int hashCode() {
        return super.hashCode() + furColor.hashCode();

    }


    @Override
    public int compareTo(Animal animal){

        int defCompare = super.compareTo(animal);
        if(defCompare!=0){
            return defCompare;
        }else if(animal instanceof Mammal) {
            return furColor.compareTo(((Mammal) animal).getFurColor());
        }else{
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Mammal) {
            return super.equals(obj) && furColor.equals(((Mammal)obj).getFurColor());
        }
        else
            return false;
    }
}
