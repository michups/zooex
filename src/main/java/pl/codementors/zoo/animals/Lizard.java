package pl.codementors.zoo.animals;

/**
 * Lizard with some scales.
 *
 * @author psysiu
 */
public class Lizard extends Animal {

    /**
     * Color of the animal scales.
     */
    private String scalesColor;

    public Lizard(String name, String scalesColor) {
        super(name);
        this.scalesColor = scalesColor;
    }

    public String getScalesColor() {
        return scalesColor;
    }

    public void setScalesColor(String scalesColor) {
        this.scalesColor = scalesColor;
    }

    @Override
    public String toString(){
        return "Lizard [Name: "+ getName()+", Color: "+ scalesColor+" ]";
    }


    @Override
    public int hashCode() {
        return super.hashCode() + scalesColor.hashCode();

    }


    @Override
    public int compareTo(Animal animal){

        int defCompare = super.compareTo(animal);
        if(defCompare!=0){
            return defCompare;
        }else if(animal instanceof Lizard) {
            return scalesColor.compareTo(((Lizard) animal).getScalesColor());
        }else{
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Lizard) {
            return super.equals(obj) && scalesColor.equals(((Lizard)obj).scalesColor);
        }
        else
            return false;
    }
}
