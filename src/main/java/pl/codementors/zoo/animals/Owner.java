package pl.codementors.zoo.animals;

/**
 * Created by michups on 14.06.17.
 */
public class Owner {

    /**
     * Animal name.
     */
    private String name;

    public Owner(String name, Type type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    private Type type;
    public enum Type { BREEDER, COLLECTOR}

    @Override
    public String toString(){
        return "Owner [Name: "+ name+", Type: "+type +" ]";
    }


    public int compareTo(Owner owner){

            return this.getName().compareTo(owner.getName());
    }

    @Override
    public int hashCode() {
        return name.hashCode();

    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj){
            return true;
        }

        if (obj instanceof Owner ) {
            return this.name.equals(((Owner) obj).getName());
        }
        else
            return false;
    }
}
